\documentclass[a4paper,11pt]{amsart}
\usepackage{./preamble}
\usepackage{marginnote}

\begin{document}
 \section{Homological Algebra}
  \marginpar{31st Mar}
  A graded $R$-module is a sequence $C_{\ast} = (C_n)_{n \in \Z}$ of $R$-modules. A map $f \colon C_{\ast} \to D_{\ast}$ is of degree p is a collection of maps $(f_n \colon C_n \to D_{n+p})_{n \in \Z}$ of $R$-module homomorphisms. We usually suppress the subscripts if the meaning is clear.
  
  Given a ring $R$, a chain complex is a pair $(C_{\ast},d)$ where $C_{\ast}$ is a graded $R$-module and $d \colon C_{\ast} \to C_{\ast}$ is a map of degree $-1$ such that $d^2 = 0$. We obtain grade $R$-modules $Z(C)$, $B(C)$, $H(C)$ where $Z(C) = \mathrm{ker}\,d$, $B(C) = \mathrm{im}\,d$ and $H(C) = Z(C) / B(C)$. These are called cycles, boundaries and homology respectively.
  
  A chain map from complexes $f \colon C_{\ast} \to C'_{\ast}$ is a graded $R$-module homomorphism of degree $0$ such that $d'f = df$. That is all of these squares commute.
  \[
   \xymatrix{ \cdots \ar[r]^{d} & C_{n+1} \ar[d]^{f} \ar[r]^d & C_{n} \ar[d]^{f} \ar[r]^d & C_{n-1} \ar[d]^{f} \ar[r]^d & \cdots \\
   \cdots \ar[r]^{d'} & C'_{n+1}  \ar[r]^{d'} & C'_{n} \ar[r]^{d'} & C'_{n-1} \ar[r]^{d'} & \cdots   }
  \]
  \begin{defn}
   A \emph{homotopy} $h$ between chain maps $f,g \colon C_{\ast} \to C'_{\ast}$ is a graded module homomorphism $h \colon C_{\ast} \to C'_{\ast}$ of degree 1 such that $d'h + hd' = f-g$. That is we have the following diagram
   \[
     \xymatrix{ \cdots \ar[r]^{d} & C_{n+1} \ar[dl]_{h} \ar@<-.5ex>[d]_{g} \ar@<.5ex>[d]^{f} \ar[r]^d & C_{n} \ar[dl]_{h} \ar@<-.5ex>[d]_{g} \ar@<.5ex>[d]^{f} \ar[r]^d & C_{n-1} \ar[dl]_{h} \ar@<-.5ex>[d]_{g} \ar@<.5ex>[d]^{f} \ar[r]^d & \cdots \\
   \cdots \ar[r]_{d'} & C'_{n+1}  \ar[r]_{d'} & C'_{n} \ar[r]_{d'} & C'_{n-1} \ar[r]_{d'} & \cdots   }
   \]
  \end{defn}
  \begin{danger}
    This diagram could be misleading as the triangles do not commute. That is $d'h \neq hd$ and $d'h \neq g ,f$ etc.
  \end{danger}
  
  \begin{prop}[{\cite[Proposition 0.1.]{Brown1982_cohomology_of_groups}}]
   A chain map $f \colon C_{\ast} \to C'_{\ast}$ induces a map $H(f) \colon H(C_{\ast}) \to H(C'_{\ast})$ and $H(f) = H(g)$ if $f$ and $g$ are homotopic.
  \end{prop} 
  The first follows because $d'f = fd$ and the second part isn't hard to show.

  \begin{exmp}[Topological motivation]
  Consider a CW-complex $X$. A singluar $n$-simplex is a continuous map $\sigma \colon \Delta^n \to X$ and let $C_n(X)  = \bigoplus_{\sigma \text{ is } n-\text{simplex}} \Z_{\sigma}$, the free-abelian group with basis the set of $n$-simplices. We can form the differential $d \colon C_n(X) \to C_{n-1}(X)$ that takes the $n$-simplices $\sigma$ to alternating sums of the $(n-1)$-simplices on the boundary of $\sigma$ \cite[Section 2.]{Hatcher2002_algebraic_topology}. As $C_n(X)$ is a free abelian group, it is a free $\Z$-module and so $C_{\ast}(X) = (C_n(X))_{n \in \N}$ is a graded $\Z$-module.
  
  Suppose $f ,g \colon X \to Y$ are homotopic. That is there exists $H \colon X \times [0,1] \to Y$ such that $H(x,0) = f(x)$ and $H(x,1) = g(x)$ for all $x \in X$.
  
  Given a continuous map $f \colon X \to Y$ we have an induced map $f_{\ast} \colon C_n(X) \to C_n(Y)$ where $\sigma \mapsto f \circ \sigma$ for all $n \in \N$. Hence we have chain maps $f_{\ast} \colon C_{\ast}(X) \to C_{\ast}(Y)$. The homotopy gives the following diagram
  \[
   \xymatrix{\Delta^n \times I \ar@{=}[d] \ar^{\sigma \times \mathrm{id}}[r] & X \times I \ar^{H}[r] & Y \\
   \Delta^{n+1} \ar[ur] \ar@{.>}[urr]_{h(\sigma)}
   }
  \]
  where $\Delta^n \times I$ is homeomorphic to $\Delta^{n+1}$.   Hence we have that $h \colon C_n(X) \to C_{n+1}(Y)$, $\sigma \mapsto h(\sigma)$ is the homotopy of chain maps in the above sense. It requires a bit more work to show that $d'h + hd' = f_{\ast}-g_{\ast}$ but see \cite[Theorem 2.10.]{Hatcher2002_algebraic_topology} for the details.   
  \end{exmp}
  
  
  \begin{defn}
   A chain map $f \colon C_{*} \to C'_{*}$ is a \emph{homotopy equivalent} if there exists $g \colon C'_{*} \to C_{*}$ such that $fg \simeq \mathrm{id}_{C'_{*}}$ and $gf \simeq \mathrm{id}_{C_{*}}$. A chain map $f \colon C_{*} \to C'_{*}$ is called \emph{weak equivalence} if $H(f) \colon H(C_{*}) \to H(C_{*}')$ is an isomorphism.
  \end{defn}
  \begin{rem}
   If two chains complexes $C_*$ and $C'_*$ are homotopic equivalent then there is an isomorphism of graded $R$-modules $H(C_*) = H(C'_*)$.
  \end{rem}

  \begin{defn}
   A chain complex $C_*$ is \emph{exact} (or \emph{acyclic}) if $H_n(C_*) = 0$ for all $n \in \Z$. A chain complex $C_*$ is \emph{contractible} if $C_* \simeq 0_*$ where $0_*$ is the zero complex
  \end{defn}
  
  \begin{rem}
   It is obvious that contractible implies exact because the zero complex is acyclic. The converse is not true. Consider the complex
   \[
    \xymatrix{0 \ar[r] & \Z \ar[r]^2 & \Z \ar[r] & \Z / 2\Z \ar[r] & 0.
    }
   \]
   Denote this complex by $C_*$. This is short exact so the chain complex is acyclic. Suppose the chain complex is homotopic equivalent to the $0$ complex. This means there exists $f \colon C_* \to 0_*$ and $g \colon 0_* \to C_*$ such that $fg \simeq \mathrm{id}_{0_*}$ and $gf \simeq \mathrm{id}_{C_*}$. Obviously $fg$ is equal to the zero map and so in particular $fg = 0$. The map $gf$ is also the zero map and so being homotopically equivalent to the zero complex is equivalent to showing that $\mathrm{id}_{C_*} \simeq 0$
   
   This means there exists a homotopy $h$ such that $dh + hd = \mathrm{id}_{C_*}$. This means we have the following diagram
   \[
    \xymatrix{0 \ar[r] & \Z \ar[dl] \ar[r]^2 & \Z \ar[dl]_m \ar[r] & \Z / 2\Z \ar[dl]_0 \ar[r] & 0 \ar[dl]\\
    0 \ar[r] & \Z \ar[r]^2 & \Z \ar[r] & \Z / 2\Z \ar[r] & 0    }
   \]
   There are two places where a contradiction turns up but they are essentially the same contradiction. As $h$ is a homomorphism from $\Z$ to $\Z$ it must be a multiplication by an integer $m$. So this means that $2m = \mathrm{id}_{\Z}$ but this clearly can't happen in $\Z$. Also any homomorphism from $\Z / 2\Z$ to $\Z$ must be the zero homomorphism. Hence we have the same contradiction.
  \end{rem}
  
  There is an important converse to keep in mind.
  \begin{prop}
   A chain complex $C_*$ is contractible if and only if it is acyclic and each short exact sequence
   \[
   \xymatrix{
    0 \ar[r] & Z_{n+1} \ar[r] & C_{n+1} \ar[r]^{\overline{d}} & Z_n \ar[r] & 0}
   \]
   splits, where $\overline{d}$ is induced by $d$.
  \end{prop}
  Essentially one uses the splitting to build a contracting homotopy. A short exact sequence of chain complexes is the following commuting diagram
  \[
   \xymatrix{ & \vdots \ar[d] & \vdots \ar[d] & \vdots \ar[d]\\
   0 \ar[r] & C'_{n+1} \ar[d]^{d'} \ar[r]^{\iota} & C_{n+1} \ar[d]^{d} \ar[r]^{\pi} & C''_{n+1} \ar[d]^{d''} \ar[r] & 0\\
   0 \ar[r] & C'_{n} \ar[d]^{d'} \ar[r]^{\iota} & C_{n} \ar[d]^{d} \ar[r]^{\pi} & C''_{n} \ar[d]^{d''} \ar[r] & 0\\
   0 \ar[r] & C'_{n-1} \ar[d]^{d'} \ar[r]^{\iota} & C_{n-1} \ar[d]^{d} \ar[r]^{\pi} & C''_{n-1} \ar[d]^{d''} \ar[r] & 0\\
   & \vdots & \vdots & \vdots.
   }
  \]
We shorten this to
\[
 \xymatrix{ 0 \ar[r] & C'_* \ar^{\iota}[r] & C_* \ar^{\pi}[r] & C''_* \ar[r] & 0
 }
\]

  
  \begin{prop}
   A short exact sequence $ \xymatrix{0 \ar[r] & C'_* \ar^{\iota}[r] & C_* \ar^{\pi}[r] & C''_* \ar[r] & 0 }$ of chain complexes gives rise to a long exact sequence in homology
   \[
    \xymatrix{ \cdots \ar[r] & H_n(C'_*) \ar^{H(\iota)}[r] & H_n(C_*) \ar^{H(\pi)}[r] & H_n(C''_*) \ar^{\partial}[r] & H_{n-1}(C'_*) \ar[r] & \cdots.
    }
   \]
Furthermore, the connecting homomorphism is natural in the usual sense.
  \end{prop}
  Let $x'' \in C''_n$ and suppose it belongs to $\mathrm{ker}\, d''$. The connecting homomorphisms follows the following diagram
  \[
   \xymatrix{& & & C''_{n+1} \ar[d] \\
   & & C_n \ar^{\pi}[r] \ar^{d}[d] & C''_n \ar^{d''}[d] \ar[r] & 0 \\
   0 \ar[r] & C'_{n-1} \ar^{d'}[d] \ar^{\iota}[r] & C_{n-1} \ar^{d}[d] \ar^{\pi}[r] & C''_{n-1} \ar[r] & 0\\
   0 \ar[r] & C'_{n-2} \ar^{\iota}[r] & C_{n-2}
   }
  \]
As $\pi \colon C_n \to C''_n$ is a surjection choose $x \in C_n$ such that $\pi(x) = x''$. As the squares commute it follows that $\pi d (x) = d'' \pi(x) = d''(x'') = 0$. Therefore $d(x) \in \mathrm{ker} \, \pi$. Hence $d(x) \in \mathrm{im} \, \iota = \mathrm{ker} \, \pi$. As $\iota \colon C'_{n-1} \to C_{n-1}$ is an injection there exists an unique $x' \in C'_{n-1}$ such that $\iota (x') = d(x)$. As $\iota d ' x' = d \iota x' = d^2(x) = 0$ and $\iota$ is an injection it follows that $x' \in \mathrm{ker} \, d'$.

Define $\partial(x'') = x'$. It takes a fair bit fiddly of work to show that $\partial$ is well-defined. That is it does not depend on the choices that have been made. We also need to show if $x'' + \mathrm{im} \, d'' = y'' + \mathrm{im} \, d''$ then $\partial(x'') + \mathrm{im} \, d' = \partial (y'') + \mathrm{im} \, d'$. This would mean that $\partial$ is a well defined map $\partial \colon H_{n}(C''_*) \to H_{n-1}(C'_*)$. Then one must show that $\partial$ is indeed a homomorphism, $\mathrm{ker} \, \partial = \mathrm{im} \, H(\pi)$ and $\mathrm{im} \, \partial = \mathrm{ker} \, H(\iota)$ and is also ``natural''.

\begin{cor}
 The inclusion $\iota \colon C'_* \to C_*$ is a weak equivalence if and only if $C''_*$ is acyclic.
\end{cor}

\begin{proof}
 As $C''_*$ is acyclic we have the exact sequence
 \[
  \xymatrix{ 0 \ar^{\partial}[r] & H_n(C'_*) \ar^{H(\iota)}[r] & H_n(C_*) \ar^{H(\pi)}[r] & 0
  }
 \]
 which is sufficient for $H(\iota)$ to be an isomorphism.
\end{proof}

Given a chain map $f \colon C'_* \to C_*$, the mapping cone $\mathrm{Cone} \, f$ is a chain complex that gives rise to a long exact sequence
\[
    \xymatrix{ \cdots \ar[r] & H_n(C'_*) \ar^{H(f)}[r] & H_n(C_*) \ar[r] & H_n(\mathrm{Cone} \, f) \ar[r] & H_{n-1}(C'_*) \ar[r] & \cdots.
    }
\]
In particular $f$ is a weak equivalence if and only if $\mathrm{Cone}\, f$ is acyclic
  \section{Resolutions and $K(G,1)$}
    \marginpar{12th May}

  Given a ring $R$ and a group homomorphism $f \colon G \to R^{\times}$, there is a unique extension of $f$ to a ring homomophism $\Z G \to R$, where $R^{\times}$ is the group units. Thus we have that $\mathrm{Hom}_{\mathrm{rings}}(\Z G, R) = \mathrm{Hom}_{\mathrm{groups}}(G,R^{\times})$
  \begin{defn}  
   A \emph{$G$-module} consists of an abelian group $A$ and a homomorphism $G \to \mathrm{Aut}(A)$. By the adjunction formula, this extends to a map $\Z G \to \mathrm{End}(A)$ and $A$ becomes a $\Z G$-module.
  \end{defn}
  
  When $G$ acts on a set $X$, one can form $\Z X = \oplus_{x \in X} \Z_x$, the free abelian group generated by $X$. We extend the action of $G$ on $X$ to an action $G \to \mathrm{Aut}(\Z X)$ where $G$ permutes the basis elements of $\Z X$. 
  
  The disjoint union corresponds to the direct sum operation in the category of $G$-modules. So $\Z[\sqcup_{i \in I} X_i] = \oplus_{i \in I} \Z X_i$. In particular if $G$ acts on $X$ then $X$ is a disjoint union of orbits and if $\set{x_i}_{i \in I}$ are orbit representatives then $\Z X = \bigoplus_i \Z [G x_i]$. However by the orbit-stabiliser theorem there is a natural bijection between $G x_i$ and $G / \mathrm{Stab}(x_i)$. Hence $\Z X = \oplus_i \Z [G / \mathrm{Stab}(x_i)]$. Confusingly we will sometimes refer to $\mathrm{Stab}(x_i)$ as $G_{x_i}$.
  
  \begin{prop}[{\cite[Proposition 3.1.]{Brown1982_cohomology_of_groups}}]
   Suppose $G$ acts freely on a set $X$ and let $E$ be a set of representatives for the $G$-orbits in $X$. Then $\Z X$ is a free $\Z G$-module  with basis $E$.
  \end{prop}
  
  \begin{proof}
   An action is free if and only if $G_x = \set{1}$ for all $x \in G$. Hence $\Z X = \oplus_i \Z G$ where $i$ ranges over orbit representatives.
  \end{proof}
  
A nice way to obtain free resolutions over $\Z$ is via topology. A \emph{$G$-complex} will be a $CW$-complex $X$ with an action of $G$ which permutes the cells. Formally a $G$-complex consists of a $G$-space $X$ and a filtration $X^0 \subset X^1 \subset X^2 \subset \cdots \subset X$ of by $G$-subspaces such that
\begin{enumerate}
 \item Each $X^n$ is closed in $X$.
 \item $\cup_{n \in \N} X^n = X$.
 \item $X^0$ is discrete subspace.
 \item For each $n \geq 1$ there is a discrete $G$-space $\Delta_n$ together with $G$-maps $f \colon S^{n-1} \times \Delta_n \to X^{n-1}$ and $\hat f \colon D^n \times \Delta_n \to X^n$ such that the following diagram is a pushout
 \[
  \xymatrix{S^{n-1} \times \Delta_n \ar[d] \ar[r]^{f} & X^{n-1} \ar[d]\\
  D^n \times \Delta_n \ar[r]^{\hat f} & X^n}
 \]
\item A subspace $Y \subset X$ is closed if and only if $Y \cap X^n$ is closed for all $n \in \N$.
\end{enumerate}


We denote $C_n(X)$ to be the free abelian group generated by the $n$-cells of $X$. We extend the action to $G \to \mathrm{Aut}(C_n(X))$ by permuting basis elements and so $C_n(X)$ becomes a $\Z G$-module. Thus if $G$ acts freely on $X$ then $C_n(X)$ is a \emph{free} $\Z G$-module. If $X$ is contractible then homology groups vanish so in particular the sequence
\[
 \xymatrix{\cdots \ar[r] & C_n(X) \ar[r]^{d} & C_{n-1}(X) \ar[r] & \cdots \ar[r] & C_0(X) \ar[r]^{\varepsilon} & \Z \ar[r] & 0
 }
\]
is a free resolution of $\Z$ over $\Z G$ when $G$ acts freely on $X$, where $d$ are the usual boundary maps and $\varepsilon$ is the augmentation map.

Given a group $G$ there always exists a $CW$-complex $Y$ such that $\pi_1(Y) = G$ and $\pi_n(Y) = 0$ for all $n > 1$. Informally this is done by taking a presentation for $G = \langle S \mid R \rangle$. Then form a bouquet of $S$ circles labelled by $S$. Then glue in 2-cells along the relations of $G$. Then one keeps gluing in higher $n$-cells to kill off high homotopy groups. For more details see \cite[Example 4.17. and pg 365]{Hatcher2002_algebraic_topology}. This space is called $K(G,1)$ and the universal cover will be contractible \cite[Theorem 4.41.]{Hatcher2002_algebraic_topology} and inherits a $CW$-complex from $K(G,1)$. As $\pi_1$ always acts freely by deck transformations on the universal cover we now have a contractible $G$-complex which $G$ acts freely on.

For example when $G = \Z_2$ then $K(G,1) = \R \mathrm{P}^{\infty}$ and the universal cover is $S^{\infty}$ \cite[Example 1B.3.]{Hatcher2002_algebraic_topology}. As a free abelian group, $C_n(S^{\infty}) = \Z \oplus \Z$ for all $n \geq 0$. However there is only one orbit in each dimension so for all $n \geq 0$, $C_n(S^{\infty}) = \Z G$ as a $G$-module. 

In the odd dimension the $\Z_2$ action preserves the orientation because it is a combination of even reflections and in the even dimension the $\Z_2$ action changes the orientation because it is a combintation of odd reflections.

%Informally an orientation in $\R^n$ is a equivalence class on the set of bases. That is let $e_1, \ldots e_n$ and $f_1, \ldots, f_n$ be basis. We say that $e_1, \ldots, e_n$ and $f_1, \ldots f_n$ have the same orientation if the determinant of the matrix sending one basis to another is positive and they have different orientation if the matrix has negative determinant. This splits the bases of $\R^n$ into two equivalence classes. An orientation is simply picking one of the two classes. An orientation on a vector bundle $\pi \colon E \to B$ consists of a collection of orientations on each fibre such that if $t \colon \pi^{-1}(U) \to U \times \R^n$ is an equivalence, and the fibres of $U \times \R^n$ are given the standard orientation, then $t$ is either orientation preserving or orientation reversing on all the fibres.




The $G$-complex has two cells in each dimension and $\Z_2$ acts by swapping the two of them. Hence we have the free resolution
\[
 \xymatrix{ \cdots \ar[r] & \Z G \ar[r] & \cdots \ar[r] & \Z G \ar[r] & \Z G \ar[r] & \Z \ar[r] & 0
 }
\]


Another example is when $G = \Z \times \Z$. Then the torus is a $K(G,1)$ and has universal cover $\R^2$.

\begin{exer}[Chapter 1, Section 3, Exercise 1.]
 Let $H$ be a subgroup of $G$ and let $E$ be a set of representatives for the right cosets $Hg$. Show that $\Z G$ regarded as a left-module over its subring $\Z H$ is free with basis $E$.
\end{exer}

\begin{proof}
 $H$ acts freely on $G$ by left translation. Hence $\Z G = \oplus \Z[H / H_x]$ where $x$ ranges over the set of representatives for the $H$-orbits. This is precisely the size of $E$. As $H$ acts freely on $G$ this means that $H_x$ will be trival. Hence $\Z G = \oplus_{x \in E} \Z H$
\end{proof}

\begin{exer}[Chapter 1, Section 3, Exercise 2.]
 Let $\varepsilon \colon \Z G \to \Z$ be the augmentation map and let $I = \mathrm{ker} \, \varepsilon$. Let $S$ be a set of elements of $G$ such that the elements $s - 1$ generates $I$ as a left ideal. Show that $S$ generates $G$.
\end{exer}

\begin{proof}
 Let $H$ be the subgroup generated by $S$. Let $x \in \Z[G /H]$ denote the element $H$ in $\Z[G/H]$. Then $h x = x$ for all $h \in H$. In particular $(s - 1) x = 0$ for all $s \in S$. As $(s-1)$ generates $I$ it follows that for all $r \in I$, $rx = 0$. In particular for all $g \in G$, $(g -1) x = 0$. Hence $gH = H$ for all $g \in G$ and so $\Z[G/H] = \Z$ and so $G = H$.
\end{proof}

\subsection{The standard resolution and $K(G,1)$}
We present a different construction of $K(G,1)$ which is homotopically equivalent to the one we constructed above. This will more probably be more useful than the construction above.

Much of this can be found in \cite[Example 1B.7.]{Hatcher2002_algebraic_topology}. Let $EG$ be the the simplicial complex whose $n$-simplices are ordered $(n+1)$-tuples $[g_0,g_1, \ldots, g_n]$. That is the vertices are labelled by $g_i$ and the edges are oriented so $g_i$ points to $g_j$ when $i < j$. This is contractible by a homotopy that slides each point $x \in [g_0, \ldots, g_n]$ along the line segment $[e,g_0,\ldots, g_n]$ to the vertex $[e]$.

Explicitly $C_n(EG)$ is the free $\Z$-module generated by the $(n+1)$-tuples $[g_0, \ldots, g_n]$ of elements of $G$, with the $G$-action $g \cdot [g_0, \ldots, g_n] = [gg_0,\ldots, gg_n]$ and boundary operator $\partial \colon C_{n}(EG) \to C_{n-1}(EG)$ where $\partial = \sum_{i=0}^n (-1)^i d_i$ where
\[
 d_i[g_0, \ldots, g_n] = [g_0, \ldots, \hat{g_i}, \ldots, g_n]
\]
$G$ clearly acts freely on $EG$ and so a basis for $C_n(EG)$ are orbit representatives of the action $G$ on the $n$-simplices $[g_0, \ldots, g_n]$. Clearly any orbit has a representative of the form $[1,g_1,g_1g_2, \ldots, g_1g_2 \ldots g_n]$ which we write in ``bar notation'', $[g_1|g_2|\cdots|g_n]$.

The quotient map $EG \to EG / G$ is the universal cover of the orbit space $BG = EG / G$ and $BG$ is a $K(G,1)$. What is nice about this contruction si that it is functorial. That is if $f \colon G \to H$ is a homomorphism then this induces a map $Bf \colon BG \to BH$ where $[g_1|\cdots|g_n] \mapsto [f(g_1)|\cdots|f(g_n)]$.

\begin{thm}[{\cite[Theorem 1B.8.]{Hatcher2002_algebraic_topology}}]
 The homotopy type of a CW-complex  $K(G,1)$ is uniquely determined by $G$
\end{thm}

\section{Free actions on Spheres}
\marginpar{20th May}

Let $X$ be a free $G$-CW complex which is homeomorphic to an odd dimensional sphere $S^{2k-1}$. Then necessarily $G$ is finite because if $G$ acts freely on the $G$-CW complex then there are at least as many $n$-cells as there are elements in $G$, for all $n \geq 0$. Since $X$ is compact, there are only finitely many cells in each dimension so $G$ must be finite.

We take sequence of free $\Z$-modules,
\[
 \xymatrix{0 \ar[r] & C_{2k-1}(X) \ar[r] & \cdots \ar[r] & C_1(X) \ar[r] & C_0(X) \ar[r]^{\varepsilon} & \Z \ar[r] & 0
 }
\]
Unfortunately, this is not an exact sequence because $H_{2k-1} (X) = \Z$ \cite[Corollary 2.14.]{Hatcher2002_algebraic_topology}. We have a map $\eta \colon \Z \to C_{2k-1}(X)$ where $1$ maps to a generator of $H_{2k-1}(X) \subset C_{2k-1}(X)$. By the Lefschetz fixed-point theorem, $G$ acts trivially on $H_{2k-1}(X)$, hence $\eta \colon \Z \to C_{2k-1}(X)$ is a $G$-map where $G$ acts trivially on $\Z$. 
\begin{danger}
 This is the importance of \emph{odd} dimension. We will see that we can't do this for an action on an even dimensional sphere.
\end{danger}


Therefore we have an exact sequence of $G$-modules,
\[
 \xymatrix{0 \ar[r] & \Z \ar[r]^{\eta} & C_{2k-1} \ar[r] & \cdots \ar[r] & C_1 \ar[r] & C_0 \ar[r]^{\varepsilon} &\Z \ar[r] & 0
 }
\]
However this isn't a free resolution of $\Z G$-modules. So what we do, is the following
\[
 \xymatrix{\cdots \ar[r] & C_1 \ar[r] & C_0 \ar[r]^{\eta \varepsilon} &C_{2k-1} \ar[r] & \cdots \ar[r] & C_1 \ar[r] & C_0 \ar[r]^{\varepsilon} & \Z \ar[r] & 0.
 }
\]
This is exact because $\eta$ and $\varepsilon$ are injective and surjective respectively.

\begin{exmp}
 Let $G$ be a finite cycle group $\langle t \mid t^n = 1 \rangle$. Decompose $S^1$ as a $CW$-complex with $n$ vertices and $n$ 1-cells. This acts freely via a rotation of $\frac{2\pi}{n}$. We have the resolution
 \[
  \xymatrix{ 0 \ar[r] & H_1(S^1)\cong \Z \ar[r]^-N  & \Z^n \cong \Z G \ar[r]^-{t-1} & \Z^n \cong \Z G \ar[r]^-{\varepsilon} & \Z \ar[r] & 0.
  }
 \]
 Suppose $C_1$ is generated by an edge $e$ and $C_0$ is generated by a vertex $v$. Then $\partial(e) = tv - v = (t-1) v$. $H_1(S^1)$ is generated by a loop say, $e + te + t^2e + \cdots + t^{n-1}e = (1 + t + \cdots + t^{n-1})e = Ne$. Hence the composition $\eta \varepsilon \colon \Z G \to \Z G$ sends $v \mapsto Ne$. Hence the free resolution is
 \[
  \xymatrix{ \cdots \ar[r] & \Z G \ar[r]^-{N} & \Z G \ar[r]^-{t-1} & \Z G \ar[r]^-N & \Z G \ar[r]^-{t-1} & \Z G \ar[r]^{\varepsilon} & \Z \ar[r] & 0
  }
 \]
\end{exmp}

\begin{thm}[Special case of Lefschetz fixed-point theorem] \label{thm:Lefschetz fixed-point theorem}
 Let $X$ be a $CW$-complex and $f \colon X \to X$ a map such that, for every cell $\sigma$,
 \[
  f(\sigma) \subset \bigcup_{\tau \neq \sigma, \mathrm{dim}\tau \leq \mathrm{dim} \sigma} \tau.
 \]
Define $f_i \colon \frac{H_i(X)}{\mathrm{torsion}} \to \frac{H_i(X)}{\mathrm{torsion}}$ to be the endomorphism between free abelian groups of finite rank induced by $f$. Then $\sum_{i} (-1)^i \mathrm{trace}(f_i) = 0$.
\end{thm}

The idea behind this proof is to show that if $g_i \colon C_i(X) \to C_i(X)$ is the induced endomorphism from $f$ then $\sum_{i}(-1)^i \mathrm{trace}(g_i) = \sum_{i} (-1)^i \mathrm{trace}(f_i)$. As $f$ does not fix any cell $g_i$ must have 0's down the diagonal for each $i$. Hence $\sum_{i}(-1)^i \mathrm{trace}(g_i) = \sum_{i} (-1)^i \mathrm{trace}(f_i) = 0$

\subsection{Free action on even dimensional spheres}
In this bit we show that only $\Z / 2\Z$ can act freely on an even dimensional sphere. Let $X$ be a path connected space and let $f \colon X \to X$ be a continuous map. Consider the sequence
\[
 \xymatrix{\cdots \ar[r] & C_1(X) \ar^{d}[r] & C_0(X) \ar[r] & 0.
 }
\]
We consider singular homology so $C_0(X) = \oplus_{x \in X} \Z$ and $C_1(X)$ is the free abelian group generated by continuous functions $\sigma \colon [0,1] \to X$ and $d \sigma = \sigma(1) - \sigma(0)$. As $X$ is path connected $H_0(X) = \Z$. For any point $x \in X$ let $\sigma_f$ be the path from $x$ to $f(x)$. Hence $d\sigma_f = f(x) - x$. Thus in homology $f(x) = x$. Hence the induced map between homology $f_{0} \colon H_0(X) \to H_0(X)$ is the identity.

For any $n$, 
\[
 H_k(S^n) = \begin{cases}
                          \Z & \quad \mbox{if $n=k$ or $k = 0$}\\
                          0 & \quad \mbox{otherwise}.
                         \end{cases}
\]
In particular if $f \colon S^n \to S^n$ satisfies the conditions in Theorem \ref{thm:Lefschetz fixed-point theorem} then
\[
 (-1)^0 \mathrm{trace} \, f_0 + (-1)^n \mathrm{trace} \, f_n = 0.
\]
However we know that $f_0$ is the identity so $\mathrm{trace} \, f_n = (-1)^{n+1}$. Suppose $G$ acts freely on $S^{2k}$ and let $a$ and $b$ be non-trivial elements of $G$. Let $f^a \colon S^{2k} \to S^{2k}$, $x \mapsto a x$ and $f^b \colon S^{2k} \to S^{2k}$, $x \mapsto bx$. As $G$ acts freely $f^a$ and $f^b$ satisfy the assumptions in Theorem \ref{thm:Lefschetz fixed-point theorem}. Hence $\mathrm{trace} \, f^a_{2k} = \mathrm{trace} \, f^b_{2k} = -1$. Hence $\mathrm{trace} \, f^{ab}_{2k} = 1$. This means that $f^{ab}$ fixes a cell and since $G$ acts freely, $f^{ab} = \mathrm{id}$ and $ab = 1$. Likewise $ba = 1$. Hence $a = b$. So $G$ must be $\Z / 2\Z$.

A nice consequence of all of this is that if $G$ acts freely on a sphere then it necessarily has periodic homology. This is because of the uniqueness of projective resolutions up to homotopy which we shall see next section.

\section{Projective Resolutions}
\marginpar{27th May}
\begin{defn}
  A module $P$ is \emph{projective} if every epimorphism $M' \to M$ and every morphism $P \to M$ lifts to a map $P \to M'$ such that the following diagram commutes.
  \[
   \xymatrix{
   & P \ar[d] \ar@{-->}[dl] \\
   M' \ar@{->>}[r] & M
   }
  \]
 \end{defn}
 Free modules are projective because one simple finds lifts of the basis elements and this is enough to extend to a homomorphism that makes this diagram commute. 
 \begin{lem}
  $P$ is projective if and only if whenever we have the solid arrows and the bottom row is extact then there exists a dotted arrow making the diagram commute:
  \[
   \xymatrix{
   & P \ar@{-->}[dl] \ar[d] \ar[dr]^-0 & \\
   M' \ar[r] & M \ar[r] & M''
   }
  \]
 \end{lem}
 \begin{proof}
  If $P$ is projective then it certainly satisfies this diagram. If $P$ satisfies this diagram then it certainly satisfes the exact sequence when $M'' =0$.
 \end{proof}
 
 Our goal in this section is the following theorem.
 \begin{thm}\label{thm:fundamental theorem of Homological algebra}
  Given a projective resolutions $F$ and $F'$ of a module $M$, there is an augmentation-preserving chain map $f \colon F \to F'$, unique up to homotopy, and $f$ is a homotopy equivalence.
 \end{thm}
 This will be done in two lemmas. The first one will give the contruction of the chain map and the second will construct the homotopy.
 
 \begin{lem} \label{lem:existence of chain maps for fundamental theorem of Homological algebra}
  If we are given the solid arrows and $P$ is projective and $d_2 f d = 0$ and the bottom row is exact then there exists the dotted arrow $g$ such that $d_1 g = fd$:
   \[
    \xymatrix{
    P \ar[r]^-d \ar@{-->}[d]_-{g} & Q \ar[d]^-f \\
    M' \ar[r]_-{d_1} & M \ar[r]_-{d_2}& M''
    }
   \]
  \end{lem}
  
  \begin{proof}
  Apply the diagram
  \[
    \xymatrix{
   & P \ar@{-->}[dl]_g \ar[d]^{df} \ar[dr]^-0 & \\
   M' \ar[r]_{d_1} & M \ar[r]_{d_2} & M''
   }
  \]

 \end{proof}

  \begin{lem} \label{lem:homotopy for the fundamental theorem of Homological algebra}
   Suppose we are given the solid arrows and $d_2 h d = d_2 f$. If $P$ is projective and the bottom row is exact then there exists the dotted arrow $k$ such that $d_1 k + hd = f$:
   \[
    \xymatrix{
    & P \ar@{-->}[dl]_k \ar[d]_f \ar[r]^d & Q \ar[dl]^h \\
    M' \ar[r]_{d_1} & M \ar[r]_{d_2} & M''
    }
   \]
   \end{lem}
   
   \begin{proof}
    Apply the diagram
     \[
    \xymatrix{
   & P \ar@{-->}[dl]_k \ar[d]|-{f -hd} \ar[dr]^-0 & \\
   M' \ar[r]_{d_1} & M \ar[r]_{d_2} & M''
   }
  \]    
   \end{proof}
   
   \begin{proof}[Proof of Theorem \ref{thm:fundamental theorem of Homological algebra}]
    Take two projective resolutions
    \[
     \xymatrix{
     \cdots \ar[r] & P_1 \ar@{-->}[d] \ar[r] & P_0 \ar@{-->}[d] \ar[r]^{\varepsilon} & M \ar[d]^{\mathrm{id}}\ar[r] & 0 \ar[r] & \cdots \\
     \cdots \ar[r] & P'_1 \ar[r] & P'_0 \ar[r]^{\varepsilon'} & M \ar[r] & 0 \ar[r] & \cdots
     }
    \]
    and continuously apply Lemma \ref{lem:existence of chain maps for fundamental theorem of Homological algebra} to obtain the chain maps between the resolutions. Similarly we continuously apply Lemma \ref{lem:homotopy for the fundamental theorem of Homological algebra} to obtain the homotopy between the two such maps.
   \end{proof}
   
   \begin{exer}{\cite[Chapter 1, Section 7, Exercise 1]{Brown1982_cohomology_of_groups}}
    Let $G$ be a finite cyclic group. Let $F$ be the free resolution of $\Z$ over $\Z G$ given by the free action on $S^1$ and let $F'$ be the bar resolution. Write down an augmentation preserving chain map $f \colon F \to F'$.
   \end{exer}
   \begin{proof}
    Let $G = \langle t \mid t^n = 1 \rangle$. We  do this up to $F_2$. We have the two free resolutions
    \[
     \xymatrix{
     \cdots \ar[r] & \Z G \ar[d]^{f_2} \cong \langle v \rangle \ar[r]^-N & \Z G \cong \langle e \rangle \ar[d]^{f_1} \ar[r]^-{t-1} & \Z G  \cong \langle v \rangle \ar[r]^-{\varepsilon} \ar[d]^{f_0} & \Z \ar[r] \ar[d]^{\mathrm{id}} & 0 \\
     \cdots \ar[r] & F_2 \ar[r]_-{d_2} & F_1 \ar[r]_-{d_1} & F_0 \ar[r]_-{\varepsilon} & \Z \ar[r] & 0 
     }
    \]
   where $N$ is defined as the map $v \mapsto (1 + t + \cdots t^{n-1})e = Ne$.    In order to preserve the augmentation map, clearly $f_0(v) = [ \ ]$. We need to satisfy $d_1 f_1(e) = (t-1)[ \ ]$. Choose $f_1(e) = [t]$. Then $d_1 ([t]) = t[ \ ] - [\ ]$. We need to satisfy $d_2 f_2(v) = f_1 (N e) = N[t]$. Recall that $d_2([x|y]) = x[y] - [xy] + [x]$. Hence when we consider $d_2 \colon (\Z G)^{n^2} \to (\Z G)^n$ restricted to the subspace spanned by $[1|t], [t|t], \ldots, [t^{n-1} | t]$ then we obtain a matrix
   \[
    \bordermatrix{~ & [1|t] & [t|t] & [t^2|t] & [t^3|t] & \cdots & [t^{n-1}|t] \cr
                  [1] & 1 & 0 & 0 & 0 & \vdots & -1 \cr
                  [t] & 0 & t + 1 & t^2 & t^3 & \vdots & t^{n-1} \cr
                  [t^2] & 0 & -1 & 1 & 0 & \vdots & 0 \cr
                  [t^3] & 0 & 0 & -1 & 1 & \vdots & 0 \cr
                  \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots \cr
                  [t^{n-1}] & 0 & 0 & 0 & 0 & \vdots & 1 \cr}.
   \]
The idea is that if we add together the columns then every cancels except the second row which is exactly what we wanted. So set $f_2(v) = [1|t] + [t|t] + \cdots [t^{n-1}|t]$. The next map gets a little too big to be worth the computation but the link between algebra and geometry is interesting.
   \end{proof}

   \section{Group Homology}
\marginpar{18th June}
\begin{defn}
 Let $G$ be a group and take a projective resolution of $\Z$ over $\Z G$, $P_{*} \to \Z$. The homology groups of $G$ is the homology of the chain complex
 \[
 \xymatrix{
  \cdots \ar[r] & \Z \otimes_{\Z G} P_2 \ar[r] & \Z \otimes_{\Z G} P_1 \ar[r] & \Z \otimes_{\Z G} P_0 \ar[r] & 0
 }
 \]
\end{defn}
Given a $G$-module $M$ let $M_G$ to be the quotient $M / IM$ where $I$ is the kernel of the map $\Z G \to \Z$, $g \mapsto 1$. We have that $M_G \cong \Z \otimes_G M$. This is because in general, when $M$ is a left $R$-module and $I$ is a right ideal, $R / I \otimes_R M \cong M / IM$, where $\bar{r} \otimes m \mapsto \overline{rm}$. One just applies the universal property of the tensor product.

We show that the 0th homology group is isomorphic to $\Z$. One has an exact sequence
\[
 \xymatrix{ \cdots \ar[r] & P_2 \ar[r] & P_1 \ar^{d_1}[r] & P_0 \ar^-{\epsilon}[r] & \Z \ar[r] & 0
 }
\]
which after tensoring by $\Z$,
\[
 \xymatrix{ \cdots \ar[r] & \Z \otimes_G P_2 \ar[r] & \Z \otimes_G P_1 \ar^{1 \otimes d_1}[r] & \Z \otimes_G P_0 \ar^-{1 \otimes \epsilon}[r] & \Z \otimes_G \Z \cong \Z \ar[r] & 0
 }
\]
one has that $\mathrm{im}(1 \otimes d_1) = \mathrm{ker}(1 \otimes \epsilon)$ because tensoring by $\Z$ is right exact functor. Hence $H_0 G = \frac{\Z \otimes_G P_0}{\mathrm{im}(1 \otimes d_1)} = \frac{\Z \otimes_G P_0}{\mathrm{ker}(1 \otimes \epsilon)} =  \Z$



\begin{rem}
 Suppose $M$ is a free $G$-module. That is $M = \oplus_{i \in I} \Z G$. Then $\oplus_{i \in I} \Z = \Z \otimes_G \oplus_{i \in I} \Z G $ where $(a)_i \mapsto a \otimes (1)_i$ where $(a)_i$ denotes the integer $a$ in the i-th co-ordinate. In other words if $F$ is a free $G$-module with basis $(e_i)$ then $\Z \otimes_G F$ is a free $\Z$-module with basis $(\bar e_i)$.
\end{rem}


We shall compute the homology of the finite cyclic group $\langle t \mid t^n = 1 \rangle$. Then we have the resolution
\[
 \xymatrix{\cdots \ar[r] & \Z G \ar[r]^-{t-1} & \Z G \ar[r]^-N & \Z G \ar[r]^-{t-1}& \Z G \ar[r]^-{\epsilon} & \Z \ar[r] & 0
 }
\]
where $N = 1 + t + \cdots + t^{n-1}$. After tensoring by $\Z$ we have that following chain complex
\[
 \xymatrix{\cdots \ar[r] & \Z \ar[r]^{0} & \Z \ar[r]^-n & \Z \ar[r]^0 & \Z \ar[r] & 0.
 }
\]
This map $\mathrm{id} \otimes (t-1) (1 \otimes e) = 1 \otimes tv - v = 1 \otimes v - 1 \otimes v = 0$ becomes the 0 map. While the map $\mathrm{id} \otimes N (1 \otimes v) = 1 \otimes (1 +t + \cdots + t^{n-1}) e = n(1 \otimes e)$ becomes multiplication by $n$. Hence
\[
 H_i G = \begin{cases}
          \Z & i = 0\\
          \Z / n\Z & \text{$i$ odd}\\
          0 & \text{$i$ even, $i > 0$}.
         \end{cases}
\]

Recall that the bar resolution is the chain complex $F_*$ where $F_n$ is spanned by the basis elements $\set{x[x_1|\cdots|x_n] : x,x_1,\ldots,x_n \in G}$. Hence after tensoring with $\Z$ we have that $\Z \otimes_G F_n$ is spanned by $\set{[x_1|\cdots|x_n] : x_1, \ldots, x_n \in G}$.

The next proposition uses results that we haven't covered yet. The full proof can be found in \cite[Theorem 9.52.]{Rotman2009_homological_algebra}.
\begin{prop}
 $H_1G = G / [G,G]$.
\end{prop}

\begin{proof}
 When we come across Tor we shall see that any short exact sequence of modules gives rise to a long exact sequence in (co)homology. Given the short exact sequence $\xymatrixcolsep{1em} \xymatrix{0 \ar[r] & I \ar[r] & \Z G \ar[r]^{\epsilon} & \Z \ar[r] & 0}$ we obtain a long exact sequence in homology.
 \[
 \xymatrixcolsep{1em}
  \xymatrix{\cdots \ar[r] & H_1(G, \Z G) \ar[r] & H_1(G,\Z) \ar[r]^-{\partial} & H_0(G,I) \ar[r] & H_0(G, \Z G) \ar[r]^-{\epsilon_*} & H_0(G,\Z) \ar[r] & 0 } 
 \]
Now $H_1(G,\Z G) =0$ as $\Z G$ is free, $H_0(G,I) = \Z \otimes_G I = I/I^2$, $H_0(G,\Z G) = \Z \otimes_G \Z G = \Z = \Z \otimes_G \Z = H_0 G$. Altogether we have
\[
 \xymatrix{\cdots \ar[r] & 0 \ar[r] & H_1(G,\Z) \ar[r]^-{\partial} & I/I^2 \ar[r] & \Z \ar[r]^-{\epsilon_*} & \Z \ar[r] & 0 }  
\]
Thus $\partial$ is injective. As $\epsilon$ is surjective it implies that $\epsilon_*$ is not 0 hence it is injective. Hence $\partial$ is surjective. This will be finished maybe a bit later

\end{proof}

\begin{prop}
 Let $X$ be a free $G$-complex and let $Y$ be the orbit complex $X / G$. Then $C_*(Y) \cong \Z \otimes_G C_*(X)$.
\end{prop}

\begin{proof}
 $C_*(X) / IC_*(X)$ has $\Z$-basis element for each $G$-orbit of cells which is precisely the basis elements of $C_*(Y)$
\end{proof}

\begin{exer}
If $G$ is a non-trivial finite cyclic group then $\Z$ does not admit a projective resolution of finite length over $\Z G$. 
\end{exer}

\begin{proof}
 Homology is a chain homotopy invariant. The homology of $G$ has infinite length hence it can not have a finite projective resolution.
\end{proof}

\begin{exer}
 If $G$ has torsion, that is there exists an element of finite order, then $\Z$ does not admit a projective resolution of finite length over $\Z G$.
\end{exer}

\begin{proof}
 This is the same proof as above one just needs to make the observation that if $H$ is a subgroup of $G$ then any projective $G$-module is a projective $H$-module because $\Z G$ is a free $H$-module.
\end{proof}

\section{Examples from topology}
\marginpar{25th June}
If $Y$ is a $K(G,1)$-complex with universal cover $X$ then $C_*(X)$ is a free resolution of $\Z$ over $\Z G$. By the previous proposition, $\Z \otimes_G C_*(X) \cong C_*(Y)$. Hence $H_*(G) = H_*(Y)$.

\begin{exmp}
 Let $Y$ be a bouquet of circles indexed by a set $S$. Then $Y$ is a $K(F(S),1)$-complex. Hence the chain complex $C_*(Y)$ is
 \[
  \xymatrix{ 0 \ar[r] & \oplus_s \Z \cong \Z S \ar[r]^-{0}& \Z \ar[r] & 0.
  }
 \]
 The boundary map is zero because each circle is mapped to the difference of a single vertex. Hence
 \[
  H_i(F(S)) = H_i(Y) = \begin{cases}
   \Z & \text{$i = 0$}\\
   \Z S ( = F(S)_{\mathrm{ab}})  & i = 1\\
   0 & i > 1
  \end{cases}
 \]
\end{exmp}

\begin{exmp}
 Let $G = \langle a_1,\ldots, a_g, b_1,\ldots, b_g \mid [a_1,b_1][a_2,b_2] \cdots [a_g b_g] = 1\rangle$. This is the fundamental group of a closed orientable surface of genus $g$. The universal cover $X$ of $\Sigma_g$ is the plane when $g =1$ or a tiling of the hyperbolic plane for $g > 1$. Hence $X$ is contractible so $\Sigma_g$ is a $K(G,1)$-complex.
 
 $\Sigma_g$ is the quotient of a $4g$-gon such that is labelled and orientated by $a_1, \ldots, a_g, b_1, \ldots, b_g$ so that as you read around the $4g$-gon one obtains the product of the commutators. Let $S = \set{a_1, \ldots, a_g, b_1, \ldots, b_g}$. Hence $C_*(Y)$ is the chain complex
 \[
  \xymatrix{0 \ar[r] & \Z \ar[r]^-{0} & \Z S \ar[r]^-{0} & \Z \ar[r] & 0.
   }
 \]
 The first differential is 0 because an edge gets mapped onto the difference of one element. The second differential maps the 2-cell onto the product of the commutators. But $\Z S$ is abelian so it must be 0. Hence
 \[
  H_iG = H_i Y = \begin{cases}
                  \Z & i=0,2\\
                  \Z^{2g} & i=1\\
                  0 & i > 2.
                 \end{cases}
 \]
\end{exmp}

\begin{exmp}
 Let $L$ be the Lie group $\mathrm{GL}_n(\R)$. In this example we are interested in discrete subgroups of $L$. $L$ is not contractible but there is a manifold $X$ such that $L$ acts on naturally. Let $X$ be the set
 \[
  X = \set{A \in M_{n}(\R) : \text{$A$ is symmetric and positive}}
 \]
where $A$ is positive if and only if $z^T A z > 0$ for all $z \in \R^n \setminus \set{0}$. The sum of two positive matrices are positive \cite[Lemma 2.2.3.]{Murphy1990_C_star_algebras_operator_theory} and $tA$ is positive for any $t > 0$ and $A$ positive. Hence $X$ forms an open convex subspace of $M_{n}(\R)$. In particular $X$ is contractible.

One can show that a matrix $A$ is positive if and only if there exists $B \in M_n(\R)$ such that $A = B^T B$ \cite[Theorem 2.2.5.]{Murphy1990_C_star_algebras_operator_theory}. Let $L$ act on $X$ via conjugation. So $g \cdot A = gA g^t$. Clearly this preserves positivity. If $A$ is a positive definite matrix then that means all the eigenvalues are strictly greater than 0. Hence there do not exist non-trivial solutions to the equation $A x = 0$ for $x \in \R^n$. Hence $A$ is injective so by the rank-nullity theorem, $A$ is bijective, so it is invertible and by abstract $C^*$-algebra nonsense (the functional calculus \cite[Theorem 2.1.13.]{Murphy1990_C_star_algebras_operator_theory}), $A^{-1}$ is also positive.

Hence this is a transitive action because by some abstract $C^*$-algebra nonsense (the functional calculus again), for all positive $A$ there exists $B$ that is also positive such that $A = B^2$. We usually denote $B$ by $A^{1/2}$. Hence if $A$ and $C$ are positive one can choose $g = C^{1/2}A^{-1/2}$ so that $g A g^T = C$.

So by the orbit stabiliser $X$ is homeomorphic to $L / \mathrm{Stab}(1)$. But $\mathrm{Stab}(1) = \set{g \in L : gg^T = 1} = O_n(\R)$ and this is compact. Hence $L$ acts properly on $X$, (for any $C \subset X$ compact the set $\set{g \in L : gC \cap C \neq \emptyset}$ is also compact. This is equivalent to the statement that the stabiliser of any point is compact). Proper actions are inhertied by subgroups so if $G$ is a discrete subgroup of $L$ then the stabilier of any point is \emph{finite}. Hence if $G$ is torsion free (no element has finite order) then $G$ must act freely on $X$.


\end{exmp}













 
 

 

 
 
 













  
  




\bibliographystyle{plain}
\bibliography{/home/chris/Documents/git_repos/cohomology_of_groups/bibliography_cohomology_of_groups.bib} 

\end{document}

